# ---------------------------------------------------------------
# Build mjpg-streamer
# ---------------------------------------------------------------

root-verify:
	@if [ $(UID) -ne 0 ]; then \
		$(MSG11) "You must be root." $(EMSG); \
		exit 1; \
	fi

opkg-verify:
	@if [ "$(OPKG_DIR)" = "" ] || [ ! -f $(OPKG_DIR)/opkg-build ]; then \
		$(MSG11) "Can't find opkg-build.  Try setting OPKG= to the directory it lives in." $(EMSG); \
		exit 1; \
	fi

build-verify:
	@if [ "$(CROSS_COMPILER)" = "" ] || [ ! -f $(CROSS_COMPILER) ]; then \
		$(MSG11) "Can't find cross toolchain.  Try setting XI= on the command line." $(EMSG); \
		exit 1; \
	fi
	@if [ "$(SD)" = "" ] || [ ! -d $(SD) ]; then \
		$(MSG11) "Can't find staging tree.  Try setting SD= on the command line." $(EMSG); \
		exit 1; \
	fi
	@if [ "$(TD)" = "" ] || [ ! -d $(TD) ]; then \
		$(MSG11) "Can't find target tree.  Try setting TD= on the command line." $(EMSG); \
		exit 1; \
	fi

# Retrieve package
$(MJPG_T)-get: .$(MJPG_T)-get

.$(MJPG_T)-get: 
	@if [ ! -e $(ARCDIR)/$(MJPG_ARCHIVE) ]; then \
		$(MSG) "================================================================"; \
		$(MSG2) "Retrieving files" $(EMSG); \
		$(MSG) "================================================================"; \
		mkdir -p $(ARCDIR); \
		cd $(ARCDIR) && $(GET_CMD) ; \
	else \
		$(MSG3) "MJPG source is cached" $(EMSG); \
	fi
	@touch .$(subst .,,$@)

$(MJPG_T)-get-patch: .$(MJPG_T)-get-patch

.$(MJPG_T)-get-patch: .$(MJPG_T)-get
	@touch .$(subst .,,$@)

# Unpack packages
$(MJPG_T)-unpack: .$(MJPG_T)-unpack

.$(MJPG_T)-unpack: .$(MJPG_T)-get-patch
	@mkdir -p $(BLDDIR)
	@cd $(ARCDIR) && $(UNPACK_CMD)
	@touch .$(subst .,,$@)

# Apply patches
$(MJPG_T)-patch: .$(MJPG_T)-patch

.$(MJPG_T)-patch: .$(MJPG_T)-unpack
	@if [ -d $(DIR_PATCH) ] && [ -n "$(ls -A "$(DIR_PATCH)")" ]; then \
		$(MSG) "================================================================"; \
		$(MSG2) "Patching source" $(EMSG); \
		$(MSG) "================================================================"; \
		for patchname in `ls -1 $(DIR_PATCH)/*.patch`; do \
			$(MSG3) Applying $$patchname $(EMSG); \
			cd $(MJPG_SRCDIR) && patch -Np1 -r - < $$patchname; \
		done; \
	fi
	@touch .$(subst .,,$@)

$(MJPG_T)-init: .$(MJPG_T)-init 

.$(MJPG_T)-init:
	@make build-verify
	@make .$(MJPG_T)-patch
	cd $(BLDDIR) && mv mjpg-streamer $(PK)-$(MJPG_VERSION)
	cp $(SRCDIR)/files/jacksonliam/CMakeLists.txt $(MJPG_SRCDIR)/$(RASPI_CMAKELIST)
	sed -i 's%\[STAGING\]%$(TD)%g' $(MJPG_SRCDIR)/$(RASPI_CMAKELIST)
	@touch .$(subst .,,$@)

$(MJPG_T)-config: .$(MJPG_T)-config

.$(MJPG_T)-config:
	@touch .$(subst .,,$@)

$(MJPG_T): .$(MJPG_T)

.$(MJPG_T): .$(X264_T) .$(MJPG_T)-init 
	@make --no-print-directory $(MJPG_T)-config
	@$(MSG) "================================================================"
	@$(MSG2) "Building MJPG" $(EMSG)
	@$(MSG) "================================================================"
	@cd $(MJPG_SRCDIR) && \
		PATH=$(XCC_PREFIXDIR)/bin:$(PATH) \
		CFLAGS=-I$(SD)/usr/include \
		LDFLAGS="-L$(SD)/usr/lib -L$(SD)/lib --sysroot=$(SD)/" \
		CC=$(XCC_PREFIX)-gcc \
		CXX=$(XCC_PREFIX)-g++ \
		make

# Build the package
	@touch .$(subst .,,$@)

$(MJPG_T)-files:
	@$(MSG) "================================================================"
	@$(MSG2) "MJPG Files" $(EMSG)
	@$(MSG) "================================================================"
	@ls -l $(MJPG_SRCDIR)

# Package it as an opkg 
pkg $(MJPG_T)-pkg: .$(MJPG_T) 
	@make --no-print-directory root-verify opkg-verify
	@mkdir -p $(PKGDIR)/opkg/mjpg-streamer/CONTROL
	@mkdir -p $(PKGDIR)/opkg/mjpg-streamer/usr/lib
	@mkdir -p $(PKGDIR)/opkg/mjpg-streamer/usr/bin
	@mkdir -p $(PKGDIR)/opkg/mjpg-streamer/usr/share/mjpg-streamer
	@cp -ar $(MJPG_SRCDIR)/*.so $(PKGDIR)/opkg/mjpg-streamer/usr/lib
	@cp -ar $(MJPG_SRCDIR)/mjpg_streamer $(PKGDIR)/opkg/mjpg-streamer/usr/bin
	@cp -ar $(MJPG_SRCDIR)/www/* $(PKGDIR)/opkg/mjpg-streamer/usr/share/mjpg-streamer
	@cp $(SRCDIR)/opkg/control $(PKGDIR)/opkg/mjpg-streamer/CONTROL/control
	@cp $(SRCDIR)/opkg/postinst $(PKGDIR)/opkg/mjpg-streamer/CONTROL/postinst
	@cp $(SRCDIR)/opkg/debian-binary $(PKGDIR)/opkg/mjpg-streamer/CONTROL/debian-binary
	@sed -i 's%\[VERSION\]%'`cat $(TOPDIR)/version.txt`'%' $(PKGDIR)/opkg/mjpg-streamer/CONTROL/control
	@chmod +x $(PKGDIR)/opkg/mjpg-streamer/CONTROL/postinst
	@chown -R root.root $(PKGDIR)/opkg/mjpg-streamer/
	@cd $(PKGDIR)/opkg/ && $(OPKG_DIR)/opkg-build -O mjpg-streamer
	@cp $(PKGDIR)/opkg/*.opk $(PKGDIR)/
	@rm -rf $(PKGDIR)/opkg

# Clean the packaging
pkg-clean $(MJPG_T)-pkg-clean:
	@if [ "$(PKGDIR)" != "" ] && [ -d "$(PKGDIR)" ]; then rm -rf $(PKGDIR); fi

# Clean out a cross compiler build but not the CT-NG package build.
$(MJPG_T)-clean: $(MJPG_T)-pkg-clean
	@if [ "$(MJPG_SRCDIR)" != "" ] && [ -d "$(MJPG_SRCDIR)" ]; then \
		cd $(MJPG_SRCDIR) && make distclean; \
	fi
	@rm -f .$(MJPG_T) 

# Clean out everything associated with MJPG
$(MJPG_T)-clobber: root-verify
	@rm -rf $(PKGDIR) 
	@rm -rf $(BLDDIR)/tmp 
	@rm -rf $(MJPG_SRCDIR) 
	@rm -f .$(MJPG_T)-config .$(MJPG_T)-init .$(MJPG_T)-patch \
		.$(MJPG_T)-unpack .$(MJPG_T)-get .$(MJPG_T)-get-patch
	@rm -f .$(MJPG_T) 

